import Vue from 'vue'
import Vuetify, {
  VBtn,
  VApp,
  VContent,
  VRadioGroup,
  VRadio,
  VRow,
  VCol,
  VResponsive,
  VExpandTransition
} from 'vuetify/lib'

Vue.use(Vuetify, {
  VBtn,
  VApp,
  VContent,
  VRadioGroup,
  VRadio,
  VRow,
  VCol,
  VResponsive,
  VExpandTransition
})

export default new Vuetify({
  theme: {
    dark: true
  }
})
