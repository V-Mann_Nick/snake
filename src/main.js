import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueTouch from 'vue-touch'

Vue.config.productionTip = false
Vue.use(VueTouch)

new Vue({
  vuetify,
  render: (h) => h(App)
}).$mount('#app')
